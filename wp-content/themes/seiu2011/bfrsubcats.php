<?php
/*
Archive Template: Banner-Featured-Recent +Subcats
*/ 
?>

<?php get_header(); ?>
<?php global $query_string; ?>
<div class="yui-gc">





	<div class="yui-u first maincol bfr bfr-subcats">
		<h1 class="page-title"><?php printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
		<?php $categorydesc = category_description(); if ( ! empty( $categorydesc ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . $categorydesc . '</div>' ); ?>

		<div class="bfr-b banner">
			<?php $my_query = new WP_Query($query_string . '&tag=section-banner&posts_per_page=1');
				while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="banner-img">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
			</div>
			<?php endwhile;	?>
		</div> <!-- END .bfr-b .banner -->

		<div class="bfr-fr featured-recent yui-g">	
	
			<div class="bfr-left bfr-f featured yui-u first">
				<h4 class="bfr-head">Featured Post:</h4>
				<?php $my_query = new WP_Query($query_string . '&tag=section-featured&posts_per_page=1');
					while ($my_query->have_posts()) : $my_query->the_post(); 
				?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php the_excerpt(); endwhile; ?>
			</div> <!-- END .bfr-left .featured -->
			
			<div class="bfr-right bfr-r recent yui-u">
				<h4 class="bfr-head">Recent Items:</h4>
				<?php
					$my_query = new WP_Query($query_string . '&tag=section-recent&posts_per_page=5');
					while ($my_query->have_posts()) : $my_query->the_post();
				?>
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php endwhile;	?>
			</div> <!-- END .bfr-right .recent -->
	
		</div> <!-- END .bfr-fr .featured-recent .yui-g -->




		<div class="subcats">
		<h2>Sub-Sections</h2>
			<?php 
				$this_category = single_cat_title( '', false );
				$this_category = get_cat_id ($this_category);
				wp_list_categories('orderby=name&show_count=0&title_li=&use_desc_for_title=1&depth=1&child_of='.$this_category);
			?>
		</div> <!-- END .subcats -->
		
		
		
		
		
</div> <!-- END .yui-u .first .maincol .bfr .bfr-subcats-->

<?php get_sidebar(); ?>
			
</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>