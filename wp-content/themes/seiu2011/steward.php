<?php
/**
 * The template used to display Steward Archive pages
 *
 * @package WordPress
 * @subpackage Twenty Eleven
 * @since 3.0.0
 */
?>

<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol steward">

		<?php the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_time(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		
		
			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
			</div><!-- .entry-content -->
		
			<div class="steward-info">
				<dl>
					<dt>Title</dt>
					<dd><?php echo get_post_meta($post->ID, 'title', true);	?></dd>
		
					<dt>Email</dt>
					<dd><?php echo get_post_meta($post->ID, 'emailaddress', true); ?></dd>
		
					<dt>Phone</dt>
					<dd><?php echo get_post_meta($post->ID, 'phonenumber', true); ?></dd>
		
					<dt>Language</dt>
					<dd><?php echo get_post_meta($post->ID, 'languages', true);	?></dd>
				</dl>
			</div> <!-- END .steward-info -->
		
		
			<div class="entry-utility">
				<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>
			</div><!-- .entry-utility -->
			
		</div><!-- #post-<?php the_ID(); ?> -->
		
		<?php get_template_part( 'loop', 'steward' ); ?>

	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>