<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol attachment">


<?php the_post(); ?>

		<h1 class="page-title"><?php echo get_the_title( $post->post_parent ); ?></h1> 

<div id="nav-below" class="navigation">
							<div class="nav-previous">&larr;<?php previous_image_link( false, 'Previous' ); ?></div>
							<div class="nav-next"><?php next_image_link( false, 'Next' ); ?></div>
						</div><!-- #nav-below -->



		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


			<h3 class="entry-title"><?php the_title(); ?></h3>

			<div class="entry-meta">
				<?php
					printf(__( '<span class="meta-prep meta-prep-author"> By </span> <span class="author vcard">%3$s</span>', 'twentyten'),
						get_author_posts_url( get_the_author_meta( 'ID' ) ),
						sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
						get_the_author()
					);
				?>
				<span class="meta-sep"> | </span>
				<?php
					printf( __( '<span class="meta-prep meta-prep-entry-date">Published </span> <span class="entry-date"><abbr class="published" title="%1$s">%2$s</abbr></span>', 'twentyten'),
						esc_attr( get_the_time() ),
						get_the_date()
					);
				?>
				<?php edit_post_link( __( 'Edit', 'twentyten' ), "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t" ); ?>
			</div> <!-- END .entry-meta -->

			<div class="entry-content">
				<div class="entry-attachment">

<?php if ( wp_attachment_is_image() ) : ?>

				<p class="attachment"><a href="<?php echo wp_get_attachment_url(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php	echo wp_get_attachment_image( $post->ID, array( $content_width, $content_width ) ); // max $content_width wide or high.
						?></a></p>

						<div id="nav-below" class="navigation">
							<div class="nav-previous"><?php previous_image_link( false, 'Previous' ); ?></div>
							<div class="nav-next"><?php next_image_link( false, 'Next' ); ?></div>
						</div><!-- #nav-below -->
<?php else : ?>
						<a href="<?php echo wp_get_attachment_url(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php echo basename( get_permalink() ); ?></a>
<?php endif; ?>
						</div>
						<div class="entry-caption"><?php if ( ! empty( $post->post_excerpt ) ) the_excerpt(); ?></div>

<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyten' )  ); ?>
<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>

					</div><!-- .entry-content -->

					<div class="entry-utility">
					<?php
						$tag_list = get_the_tag_list();
						if ( '' != $tag_list ) {
							$utility_text = __( '', 'twentyten' );
						} else {
							$utility_text = __( '', 'twentyten' );
						}
						printf( 
							$utility_text,
							get_the_category_list( ', ' ),
							$tag_list,
							get_permalink(),
							the_title_attribute( 'echo=0' ),
							get_post_comments_feed_link() 
						); 
					?>

<?php if ( comments_open() && pings_open() ) : // Comments and trackbacks open ?>
						<?php printf( __( '<a class="comment-link" href="#respond" title="Post a comment">Post a comment</a> or leave a trackback: <a class="trackback-link" href="%s" title="Trackback URL for your post" rel="trackback">Trackback URL</a>.', 'twentyten' ), get_trackback_url() ); ?>
<?php elseif ( ! comments_open() && pings_open() ) : // Only trackbacks open ?>
						<?php printf( __( 'Comments are closed, but you can leave a trackback: <a class="trackback-link" href="%s" title="Trackback URL for your post" rel="trackback">Trackback URL</a>.', 'twentyten' ), get_trackback_url() ); ?>
<?php elseif ( comments_open() && ! pings_open() ) : // Only comments open ?>
						<?php _e( 'Trackbacks are closed, but you can <a class="comment-link" href="#respond" title="Post a comment">post a comment</a>.', 'twentyten' ); ?>
<?php elseif ( ! comments_open() && ! pings_open() ) : // Comments and trackbacks closed ?>
						<?php _e( 'Both comments and trackbacks are currently closed.', 'twentyten' ); ?>
<?php endif; ?>
<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>
					</div><!-- .entry-utility -->
				</div><!-- #post-<?php the_ID(); ?> -->

<?php comments_template(); ?>

	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>
