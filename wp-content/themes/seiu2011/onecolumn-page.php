<?php
/*
Template Name: One column, no sidebar
Description: A template with no sidebar
*/
?>

<?php get_header(); ?>

	<div class="maincol onlycol">
	
	<?php the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-<?php the_ID(); ?> -->

	<?php comments_template( '', true ); ?>

	</div> <!-- END .maincol .onlycol-->

</div> <!-- END #bd -->
<?php get_footer(); ?>