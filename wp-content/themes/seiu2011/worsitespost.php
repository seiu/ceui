<?php
/*
Template Name: Worksites Post Template
Description: a post template worksites
*/
?>

<?php get_header(); ?>

<div class="yui-gc">
	<div class="yui-u first maincol">

		<?php the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		
		
		
			<h1 class="entry-title"><?php the_title(); ?></h1>


			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( 'before=<div class="page-link">' . __( 'Pages:', 'twentyten' ) . '&after=</div>' ); ?>
			</div><!-- END .entry-content -->
				
			<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their decscription show a bio on their entries  ?>
			
				
			<?php endif; ?>

			<div class="entry-utility">


				<?php edit_post_link( __( 'Edit', 'twentyten' ), "\n\t\t\t\t\t<span class=\"edit-link\">", "</span>" ); ?>

			</div><!-- END .entry-utility -->

		</div> <!-- END #post-<?php the_ID(); ?> -->



	</div> <!-- END .yui-u .first .maincol-->
<?php get_sidebar(); ?>

</div> <!-- END .yui-gc -->
</div> <!-- END #bd -->
<?php get_footer(); ?>